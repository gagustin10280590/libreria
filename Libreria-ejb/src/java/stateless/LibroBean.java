/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;

/**
 *
 * @author gileto
 */
@Stateless
public class LibroBean implements LibroBeanRemote
{
    @PersistenceContext(unitName = "Libreria-ejbPU")
    EntityManager em;
    protected  Libro libro;
    protected Collection<Libro > listaLibros;

    
    public void addLibro(String Titulo, String autor, BigDecimal precio)
    {
        if (libro==null) 
        {
         libro= new Libro(Titulo, autor, precio);
         em.persist(libro);
         libro=null;
        }
    }
        
public  Collection <Libro> getAllLibro()
        {
            listaLibros=(Collection<Libro>) em.createNamedQuery("Libro.findAll");
            return listaLibros;   
            
        }   

    @Override
    public Libro buscaLibro(int id) {
     libro = (Libro) em.find(Libro.class, id);        
        return libro;
    }

    public Libro actualizaLibro(Integer libr, String Titulo, String autor, BigDecimal precio) {
       libro=buscaLibro(libr);
        if (libro!=null) 
        {
            libro.setTitulo(Titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro);
           em.flush();            
        }
        return libro;
    }

    @Override
    public Collection<Libro> getAllLibros() {
         listaLibros = em.createNamedQuery("Libro.findAll").getResultList();
         return listaLibros;        
    }    

    @Override
    public void eliminaLibro(Integer id) {        
        libro=buscaLibro(id);        
        em.remove(libro);
    }

}
